#ifndef WATKI_H
#define WATKI_H

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <thread>

#include "Wspolrzedne.h"
#include "Postac.h"
#include <vector>
#include <string>
#include <sstream>

#pragma comment (lib, "Ws2_32.lib")
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"
extern SOCKET ClientSocket;

// Funkcja probuje utworzyc polaczenie z przeciwnikiem
// Zwraca true - jak polaczenie sie udalo, false - w przeciwnym wypadku
bool polacz_z_przeciwnikiem();

// Funkcja dla watku(?). Ma za zadanie wysylanie obrazu gry do gracza. Tak naprawde wysyla wpolrzedne ogonow obu graczy
// ktory nastepnie sobie je pobiera i na ich podstawie wyswietla na ekranie
void wysylanie_obrazu_gry();

// Odczytywanie ruchu gracza i przypisanie go do jego postaci - komputer
void odczytaj_ruch_przeciwnika();


#endif