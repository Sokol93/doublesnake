#include "watki.h"

extern Postac gracz, komputer;
SOCKET ClientSocket = INVALID_SOCKET;

bool polacz_z_przeciwnikiem()
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;


	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return false;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return false;
	}

	// Setup the TCP listening socket
	iResult = ::bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return false;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return false;
	}

	printf("Czekam na klienta...\n");
	// Accept a client socket
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return false;
	}
	printf("Polaczono z klientem: %d\n", ClientSocket);

	// No longer need server socket
	closesocket(ListenSocket);

	return true;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
void wysylanie_obrazu_gry()
{
	std::vector<Wspolrzedne> *ogon_gracz1;// tutaj bedziemy trzymac wskaznik na ogon gracza i gracza2
	std::vector<Wspolrzedne> *ogon_gracz2;
	std::ostringstream strumien;

	ogon_gracz1 = gracz.podajOgon(); // najpierw pokazujemy na gracza
	ogon_gracz2 = komputer.podajOgon();

	std::string wiadomosc = "";
	int dlugosc = ogon_gracz1->size();
	int dlugosc2 = ogon_gracz2->size();
	
	if (dlugosc != dlugosc2) return;

	wiadomosc += dlugosc; // Wiadomosc na poczatku podaje ile bedzie par wspolrzednych dla kazdej postaci
	strumien << dlugosc;
	// Wpisujemy do wiadomosci informacje o ogonie gracza
	for (int i = 0; i < dlugosc; ++i)
	{
		wiadomosc += " " + ogon_gracz1->at(i).X;
		wiadomosc += " " + ogon_gracz1->at(i).Y;
		strumien << " " << ogon_gracz1->at(i).X << " " << ogon_gracz1->at(i).Y;
	}

	// Zmieniamy wskazywanie na ogon gracza2 i zapisujemy informacje o jego ogonie
	//ogon_postaci = komputer.podajOgon();

	for (int i = 0; i < dlugosc2; ++i)
	{
		wiadomosc += " " + ogon_gracz2->at(i).X;
		wiadomosc += " " + ogon_gracz2->at(i).Y;
		strumien << " " << ogon_gracz2->at(i).X << " "<< ogon_gracz2->at(i).Y;

	}

	//std::cout << "STRUMIEN: " << std::endl << std::endl << strumien.str() << std::endl << std::endl << "KONICE STRUMIENIA" << std::endl << std::endl;
	// i wysylamy wiadomosc do przeciwnika
	int wynik;
	wiadomosc = strumien.str();
	//std::cout << wiadomosc << std::endl;
	wynik = send(ClientSocket, wiadomosc.c_str(), wiadomosc.length(), 0);
	if (wynik == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
void odczytaj_ruch_przeciwnika()
{
	int iResult; // zmienna na ilosc bajtow przyslanych
	const int wielkosc_buf = 16;
	char buf[wielkosc_buf];

	do
	{
		iResult = recv(ClientSocket, buf, wielkosc_buf, 0);
		if (iResult > 0){ // gdy cos przyslano
			std::string wiadomosc = std::string(buf, buf + iResult);

			if (wiadomosc == "GORA")
				komputer.skrec_w(GORA);
			else if (wiadomosc == "DOL")
				komputer.skrec_w(DOL);
			else if (wiadomosc == "PRAWO")
				komputer.skrec_w(PRAWO);
			else if (wiadomosc == "LEWO")
				komputer.skrec_w(LEWO);
			else if (wiadomosc == "koniec")
				break;
		}
		else if (iResult == 0){
			printf("Connection closing...\n");
			break;
		}
		else  {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			//return;
			break;
		}

		// ???Potrzebne jakies opoznienie by przeciwnik nie zmienial kierownkow non stop???
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	} while (iResult > 0);
}