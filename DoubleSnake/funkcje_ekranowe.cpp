#include <stdlib.h>
#define _WINSOCKAPI_
#include <Windows.h>
#include "funkcje_ekranowe.h"

// wpolrzedne kwadratu
Postac gracz("Gracz", Wspolrzedne(-10, 0), 10, DOL, 1.0, 0.0, 1.0);
Postac komputer("Gracz2", Wspolrzedne(10, 0), 10, GORA, 0.5, 1.0, 0.25/*, true, &gracz*/);
int tryb_gry; // ogresla typ gry. 1 - gracz vs komputer, 2 - pvp przez internet, 3 - pvp lokalnie

GLdouble xx = 0, yy = 0;
int wind; //zapamietanie nr okna

using namespace std;

///////////////////////////////////////////////////////////////////////////////////////
void Display()
{
	// kolor t�a - zawarto�� bufora koloru
	glClearColor(1.0, 1.0, 1.0, 1.0);

	// czyszczenie bufora koloru
	glClear(GL_COLOR_BUFFER_BIT);

	rysujPlansze();

	gracz.wyswietlPostac();
	komputer.wyswietlPostac();

	// skierowanie polece� do wykonania
	glFlush();

	// zamiana bufor�w koloru
	glutSwapBuffers();
}
///////////////////////////////////////////////////////////////////////////////////////
void SpecialKeys(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_LEFT:
		gracz.skrec_w(LEWO);
		break;

	case GLUT_KEY_UP:
		gracz.skrec_w(GORA);
		break;

	case GLUT_KEY_RIGHT:
		gracz.skrec_w(PRAWO);
		break;

	case GLUT_KEY_DOWN:
		gracz.skrec_w(DOL);
		break;

	case GLUT_KEY_F1: //Pauza
		_getch();
		break;

	default: break;
	}
}
///////////////////////////////////////////////////////////////////////////////////////
void keyPressed(unsigned char key, int x, int y)
{
	if (tryb_gry == 3)
	switch (key)
	{
	case 'a':
		komputer.skrec_w(LEWO);
		break;

	case 'w':
		komputer.skrec_w(GORA);
		break;

	case 'd':
		komputer.skrec_w(PRAWO);
		break;

	case 's':
		komputer.skrec_w(DOL);
		break;
	}
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
void Menu(int value)
{
	switch (value)
	{
	case EXIT1:
		exit(0);
		break;
	}
}
///////////////////////////////////////////////////////////////////////////////////////
void bezczynnosc()
{
	static int licznik = 0;

	if (licznik++ % 5 == 0) {
		gracz.wydluz_ogon();
		komputer.wydluz_ogon();
	}

	komputer.wykonaj_ruch();
	Display();

	gracz.wykonaj_ruch();
	Display();

	// Jezeli przeciwnik nie jest komputerem to wysylamy mu obraz gry
	if (tryb_gry == 2)
		wysylanie_obrazu_gry();
	if (tryb_gry != 1)
		std::this_thread::sleep_for(std::chrono::milliseconds(200));

	if (komputer.czyKolizja()) {
		wypiszZwyciezceIZakonczGre("Gracz 1");
		_getch();
		exit(0);
	}
	if (gracz.czyKolizja()) {
		wypiszZwyciezceIZakonczGre("Gracz 2");
		_getch();
		exit(0);
	}
}
///////////////////////////////////////////////////////////////////////////////////////
void rysujPlansze()
{
	GLfloat wymiar = 1 - jeden_piksel * 12;
	
	glColor3f(0.0, 0.0, 0.0);

	glBegin(GL_POLYGON);
	glVertex3f(-1.00, 1.00, 0.00); glVertex3f(1.00, 1.00, 0.00);
	glVertex3f(1.00, wymiar, 0.00); glVertex3f(-1.00, wymiar, 0.00);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.00, -1.00, 0.00); glVertex3f(1.00, -1.00, 0.00);
	glVertex3f(1.00, -wymiar, 0.00); glVertex3f(-1.00, -wymiar, 0.00);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.00, -1.00, 0.00); glVertex3f(-1.00, 1.00, 0.00);
	glVertex3f(-wymiar, 1.00, 0.00); glVertex3f(-wymiar, -1.00, 0.00);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(1.00, -1.00, 0.00); glVertex3f(1.00, 1.00, 0.00);
	glVertex3f(wymiar, 1.00, 0.00); glVertex3f(wymiar, -1.00, 0.00);
	glEnd();
}
///////////////////////////////////////////////////////////////////////////////////////
void wypiszZwyciezceIZakonczGre(const std::string& nazwa)
{
	string tekst("Zwyciezyl: " + nazwa + "!!!");
	ofstream plik("Log", ios::app);
	
	plik << nazwa[0] << endl;

	//boost::this_thread::sleep(boost::posix_time::milliseconds(5000));
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	glutDestroyWindow(wind);

	cout << string(79, '*') << endl
		<< "*" << string(77, ' ') << "*" << endl
		<< string(40 - tekst.length() / 2, ' ') << tekst << string(39 - tekst.length() / 2, ' ')
		<< "*" << string(77, ' ') << "*" << endl
		<< string(79, '*') << endl;

	//std::this_thread::sleep_for(std::chrono::milliseconds(5000));
}